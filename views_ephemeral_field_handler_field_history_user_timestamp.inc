<?php
/**
 * @file
 * Class definition for views ephemeral field module.
 *
 * Extends class views_handler_field_history_user_timestamp.
 */

class views_ephemeral_field_handler_field_history_user_timestamp extends views_handler_field_history_user_timestamp {
  function init(&$view, $options) {
    // Since we are completely overriding this function from that of its parent,
    // we must ensure that the parent of the parent function is called.
    views_handler_field_node::init($view, $options);

    $this->additional_fields['created'] = array('table' => 'node', 'field' => 'created');
    $this->additional_fields['changed'] = array('table' => 'node', 'field' => 'changed');
    if (module_exists('comment') && !empty($this->options['comments'])) {
      $this->additional_fields['last_comment'] = array('table' => 'node_comment_statistics', 'field' => 'last_comment_timestamp');
    }
    // Get the value of option 'ephemeral' for reference by other methods.
    $this->ephemeral['enabled'] = $this->options['ephemeral']['enabled'];
    $this->ephemeral['units'] = $this->options['ephemeral']['units'];
    $this->ephemeral['delay'] = $this->options['ephemeral']['delay'];
  }

  function option_definition() {
    $options = parent::option_definition();

    $options['ephemeral']['enabled'] = array('default' => FALSE);

    return $options;
  }

  function options_validate($form, $form_state) {
    $regex_pattern = '/^[0-9]{1,}$/';
    if ($form_state['input']['options']['ephemeral']['enabled'] == 1 && preg_match($regex_pattern, $form_state['input']['options']['ephemeral']['delay']) < 1) {
      form_set_error('options][ephemeral][delay', t('Duration of time must be an integer.'));
    }
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['ephemeral'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ephemeral'),
      '#description' => t('Content shall be marked as \'New\' or \'Updated\' for anonymous users for a set time period after the content has been created or updated. This provides a method for showing new or updated markers to anonymous users.'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    );
    $form['ephemeral']['enabled'] = array(
      '#type' => 'checkbox',
      '#title' => t('Make markers viewable to anonymous users.'),
      '#default_value' => !empty($this->options['ephemeral']['enabled']),
    );
    $form['ephemeral']['units'] = array(
      '#type' => 'select',
      '#title' => t('Units'),
      '#description' => t('Select units of time.'),
      '#options' => array(
        'mins' => t('Mins'),
        'hours' => t('Hours'),
        'days' => t('Days'),
      ),
      '#default_value' => isset($this->options['ephemeral']['units']) ? $this->options['ephemeral']['units'] : 'hours',
    );
    $form['ephemeral']['delay'] = array(
      '#type' => 'textfield',
      '#title' => t('Duration'),
      '#size' => 3,
      '#description' => t('Enter how many of units of time the marker should be displayed for anonymous users after creating or updating content.'),
      '#default_value' => isset($this->options['ephemeral']['delay']) ? $this->options['ephemeral']['delay'] : 0,
    );

  }

  function query() {
    views_handler_field_node::query();
  }

  /**
   * Overrides render function of parent class with this modified version.
   * @see views_handler_field_history_user_timestamp::render()
   */
  function render($values) {
    // Let's default to 'read' state.
    // This code shadows node_mark, but it reads from the db directly and
    // we already have that info.
    $mark = MARK_READ;
    global $user;

    $created = $values->{$this->aliases['created']};
    $changed = $values->{$this->aliases['changed']};

    $last_comment = module_exists('comment') && !empty($this->options['comments']) ? $values->{$this->aliases['last_comment']} : 0;

    if ($this->ephemeral['enabled'] == 0 || ($this->ephemeral['enabled'] == 1 && $user->uid)) {
      // Keep regular render behavior if ephemeral is not enabled or user is logged in.
      if ($user->uid) {
        $last_read = $values->{$this->field_alias};
        if (!$last_read && $created > NODE_NEW_LIMIT) {
          $mark = MARK_NEW;
        }
        elseif ($changed > $last_read && $changed > NODE_NEW_LIMIT) {
          $mark = MARK_UPDATED;
        }
        elseif ($last_comment > $last_read && $last_comment > NODE_NEW_LIMIT) {
          $mark = MARK_UPDATED;
        }
      }
    }
    elseif ($this->ephemeral['enabled'] == 1) {
      // Use modified render behaviour if ephemeral is enabled for anonymous users.

      $current_time = time();

      // Get number of days or hours in timestamp format. 1 day = 24 hrs * 60 mins * 60 secs.
      switch ($this->ephemeral['units']) {
        case 'mins':
          $units = $this->ephemeral['delay'] * (60);
          break;
        case 'hours':
          $units = $this->ephemeral['delay'] * (60 * 60);
          break;
        case 'days':
          $units = $this->ephemeral['delay'] * (24 * 60 * 60);
          break;
      }

      if ($created + $units > $current_time) {
        $mark = MARK_NEW;
      }
      elseif ($changed + $units > $current_time && $changed > NODE_NEW_LIMIT) {
        $mark = MARK_UPDATED;
      }
      elseif ($last_comment + $units > $current_time && $last_comment > NODE_NEW_LIMIT) {
        $mark = MARK_UPDATED;
      }
    }
    // Return themed and rendered field.
    return $this->render_link($this->create_mark($mark), $values);
  }

  /**
   * Update the 'last viewed' timestamp of the specified node for current user.
   *
   * Modified from node_tag_new() from file 'node.module'.
   */
  function tag_new($nid) {
    global $user;

    // If not anonymous user.
    if ($user->uid) {
      if (node_last_viewed($nid)) {
        db_query('UPDATE {history} SET timestamp = %d WHERE uid = %d AND nid = %d', time(), $user->uid, $nid);
      }
      else {
        @db_query('INSERT INTO {history} (uid, nid, timestamp) VALUES (%d, %d, %d)', $user->uid, $nid, time());
      }
    }
  }

  /**
   * Return a themed marker, useful for marking new or updated
   * content.
   *
   * @param $type
   *   Number representing the marker type to display
   * @see MARK_NEW, MARK_UPDATED, MARK_READ
   * @return
   *   A string containing the marker.
   */
  protected function create_mark($type = MARK_NEW) {
    if ($type == MARK_NEW) {
      return ' <span class="marker">'. t('new') .'</span>';
    }
    elseif ($type == MARK_UPDATED) {
      return ' <span class="marker">'. t('updated') .'</span>';
    }
  }

}
