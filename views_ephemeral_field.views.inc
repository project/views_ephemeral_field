<?php
/**
 * @file
 * Contains views specific functions.
 */

/**
 * Implements hook_views_handlers().
 */
function views_ephemeral_field_views_handlers() {
  return array(
    'handlers' => array(
      // field handlers
      'views_ephemeral_field_handler_field_history_user_timestamp' => array(
        'parent' => 'views_handler_field_history_user_timestamp',
      ),
    ),
  );
}

/**
 * Implements hook_views_data_alter().
 */
function views_ephemeral_field_views_data_alter(&$data) {
  if (isset($data['history_user']['timestamp']['field']['handler']) && $data['history_user']['timestamp']['field']['handler'] == 'views_handler_field_history_user_timestamp') {
    $data['history_user']['timestamp']['field']['handler'] = 'views_ephemeral_field_handler_field_history_user_timestamp';
  }
}
